#!/bin/sh

exec swayidle -w \
         timeout 30 'swaylock -f -c 000000' \
         timeout 60 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
         before-sleep 'swaylock -f -c 000000'
#
# This will lock your screen after 30 seconds of inactivity, then turn off
# your displays after another 30 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.
