#!/bin/BASH

sudo systemctl restart ModemManager
#sudo systemctl restart eg25-manager

if [ $? -eq 0 ]; then
    sleep 30
    sudo systemctl restart systemd-networkd; sudo systemctl restart NetworkManager
    pkill chatty
else
    echo "failed to restart modem manager"
fi
